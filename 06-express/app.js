import express from 'express'
import fs from 'fs'
import fsAsync from 'fs/promises'

const app = express();

const PORT = 3000;

app.use(express.json());

app.post('/', (req, res, next) => {
    console.log(req.body);
});

app.get('/file', (req, res) => {
    fs.readFile('/file.txt', (err, data) => {
        if (err) {
            res.sendStatus(404);
        }
    });
});

app.get('/file1', (req, res) => {
    try {
        const data = fs.readFileSync('/file.txt');
        res.send(data);
    } catch (error) {
        res.sendStatus(404);
    }
});
// promise는 항상 catch로 error 처리해야 한다.
app.get('/file2', (req, res) => {
    fsAsync
    .readFile('/file2.txt')  //
    .then((data) => res.send(data))
    .catch((error) => res.sendStatus(404));
});

app.get('/file3', async (req, res) => {
    try {
        const data = await fsAsync.readFile('/file.txt');
        res.send(data);
    } catch (error) {
        res.sendStatus(404);
    }
});

app.use((req, res, next) => {
    res.status(404).send('요청 페이지는 없습니다! 가능한 요청을 하세요!');
});

app.use((error, req, res, next) => {
    console.error(error);
    res.status(500).send('Try later!');
});

app.listen(PORT, () => {
    console.log(`${PORT} PORT Server Ready...!`)
});