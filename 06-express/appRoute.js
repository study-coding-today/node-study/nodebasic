import express from "express";
import postRouter from './router/post.js';
import userRouter from './router/user.js';

const app = express();

// REST API에서 Body
app.use(express.json());
// HTML Form에서 Body
app.use(express.urlencoded({extended: true}));
// static 미들웨어 설정
const options = {
    dotfiles: 'ignore', // 숨겨진 파일은 무시
    etag: false,
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set('x-timestamp', Date.now());
    },
};
app.use(express.static('public', options));

/* 아래와 같이 반복되는 경우 route 를 사용하면 된다.
app.get('/posts', (req, res) => {
    res.status(201).send('GET: /posts');
});

app.post('/posts', (req, res) => {
    res.status(201).send('POST: /posts');
});

app.put('/posts/:id', (req, res) => {
    res.status(201).send('PUT: /posts/:id');
});

app.delete('/posts/:id', (req, res) => {
    res.status(201).send('DELETE: /posts/:id');
});
*/

/* 위 경로가 같을 경우 아래와 같이 묶을 수 있다.
app.route('/posts')
    .get((req, res) => {
        res.status(201).send('GET: /posts');
    })
    .post((req, res) => {
        res.status(201).send('POST: /posts');
    });

app.route('/posts/:id')
    .put((req, res) => {
        res.status(201).send('PUT: /posts/:id');
    })
    .delete((req, res) => {
        res.status(201).send('DELETE: /posts/:id');
    });
*/

app.use('/posts', postRouter);
app.use('/users', userRouter);

app.listen(3000);