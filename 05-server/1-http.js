const http = require('http');
const fs = require('fs');
const ejs = require('ejs')
// const http2 = require('http2'); // https

const name = 'Jade';
const courses = [
    { name: 'Node' },
    { name: 'Js' },
    { name: 'Html' },
    { name: 'Css' },
];

const server = http.createServer((req, res) => {
    /*console.log('incoming...');
    console.log(req.headers);
    console.log(req.httpVersion);
    console.log(req.method);
    console.log(req.url);*/
    const url = req.url;
    res.setHeader('Content-Type', 'text/html');
    if (url === '/') {
        /*fs.createReadStream('./html/index.ejs').pipe(res);*/
        ejs
        .renderFile('./template/index.ejs', {name})
        .then((data) => res.end(data));
    } else if (url === '/courses') {
        /*fs.createReadStream('./html/courses.ejs').pipe(res);*/
        ejs
        .renderFile('./template/courses.ejs', {courses})
        .then((data) => res.end(data));
    } else {
        /*fs.createReadStream('./html/not-found.ejs').pipe(res);*/
        ejs
        .renderFile('./template/not-found.ejs', {name})
        .then((data) => res.end(data));
    }
});

server.listen(3000, () => {
    console.log('up Server....');
});