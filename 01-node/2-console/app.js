console.log("logging.....")
console.clear();        // 코솔에 모든 내용 지우기

/* log level */
console.log('log');         // 개발
console.info('info');       // 정보
console.warn('warn');       // 정보
console.error('error');     // 에러, 사용자 에러, 시스템 에러

/* assert 참이 아닌경우만 출력 */
console.assert(2 === 3, 'not same!');
console.assert(2 === 2, 'same!');

/* print object */
const student = {name: 'jul', age: 20, company: {name: 'xx'}};
console.log(student);
console.table(student);
console.dir(student, {showHidden: true, colors: true, depth: 2});

/* measuring time */
console.time('for loop');
for (let i = 0; i < 10; i++) {
    i++;
}
console.timeEnd('for loop');

/* counting */
function co() {
    console.count('co function');
}
co();
co();
console.countReset('co function');
co();

/* trace */
function f1() {
    f2();
}

function f2() {
    f3();
}

function f3() {
    console.log('f3');
    console.trace();
}

f1();