const EventEmitter = require('events');
const emitter = new EventEmitter();
const callback1 = (args) => {
    console.log('first callback : ', args);
};

emitter.on('jeong', callback1);

emitter.on('jeong', (args) => {
    console.log('second callback : ', args);
});

// event 'jeong'을 만들어 그 object 데이터를 보낸다.
emitter.emit('jeong', { message: 1 });
emitter.emit('jeong', { message: 2 });
/*변수로 선언된 callback1을 제거한다.
emitter.removeListener('jeong', callback1);*/
// 모든 event 를 제거한다.
emitter.removeAllListeners();
emitter.emit('jeong', { message: 3 });
