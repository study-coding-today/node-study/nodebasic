const path = require('path');

console.log(__dirname);
console.log(__filename);

/* POSIX (Unix: Mac, Linux) : 'User/temp/file.exe'
* Windows : 'C:\\temp\\file.exe; */

console.log(path.sep);  // 경로 구분자
console.log(path.delimiter);    // 환경변수 구분자

/* basename : 파일가져오기 */
console.log(path.basename(__filename));
console.log(path.basename(__filename, '.js'));

/* dirname 가져오기 */
console.log(path.dirname(__filename));

/* extension : 확장자 가져오기*/
console.log(path.extname(__filename));

/* parse 가져오기 */
const parsed = path.parse(__filename);
console.log(parsed);
console.log(parsed.root);
console.log(parsed.name);

const str = path.format(parsed);
console.log(str);

/* isAbsolute 절대경로 인가? */
console.log('isAbsolute?', path.isAbsolute(__dirname));
console.log('isAbsolute?', path.isAbsolute('../'));

/* normalize : 잘못된 경로 수정 */
console.log(path.normalize('./folder////sub'));

/* join */
console.log(__dirname + '/' + 'image');
console.log(__dirname + path.sep + 'image');
console.log(path.join(__dirname, 'image'));

