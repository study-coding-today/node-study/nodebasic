                                                                            const fs = require('fs').promises;

/* file read */
fs.readFile('./10-file/text.txt', 'utf8')
.then((data) => console.log(data))
.catch(console.error);

/* file write */
fs.writeFile('./10-file/text.txt', 'JAVASCRIPT GOOD!!')
.catch(console.error);

fs.appendFile('./10-file/text.txt', 'GOOD NODEJS!')
.then(() => {
    /* copy */
    fs.copyFile('./10-file/text.txt', './10-file/text2.txt')
    .catch(console.error);
})
.catch(console.error);

/* folder */
fs.mkdir('sub_folder')
.catch(console.error);

fs.readdir('./10-file/')
.then(console.log)
.catch(console.error);