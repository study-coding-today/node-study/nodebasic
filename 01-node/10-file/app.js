const fs = require('fs');

/* API 3가지 형태
* 1. rename(...., callback(error, data)); 비동기
* 2. try { renameSync(....) } catch(e) { } 동기
* 3. promises.rename().then().catch()
* */

/* 동기
try {
    fs.renameSync('./10-file/test.txt', './10-file/test-new.txt');
} catch (error) {
    console.error(error);
}
console.log('renameSync');
*/


/* 비동기
fs.rename('./10-file/test-new.txt', './10-file/test.txt', (error) => {
    console.log(error);
});
console.log('rename');
 */

fs.promises
  .rename('./10-file/test.txt', './10-file/test-new.txt')
  .then(() => console.log('Done!'))
  .catch(console.error);
