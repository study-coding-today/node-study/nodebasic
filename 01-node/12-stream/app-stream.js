const fs = require('fs');

/*
const readStream = fs.createReadStream('./12-stream/file.txt', {
    highWaterMark: 64,   // 기본 64 kbytes 읽고 쓴다.
    encoding: 'utf-8',
});
*/

/*
readStream.on('data', (chunk) => {
    // console.log(chunk);
    console.count('data');
});

readStream.on('error', (error) => {
    console.log(error);
});*/

const data = [];
/*
readStream.on('data', (chunk) => {
    // console.log(chunk);
    data.push(chunk);
    console.count('data');
});

readStream.on('end', () => {
    console.log(data.join(''));
});

readStream.on('error', (error) => {
    console.log(error);
});*/

/*readStream.on('data', (chunk) => {
    // console.log(chunk);
    data.push(chunk);
    console.count('data');
});

readStream.on('end', () => {
    console.log(data.join(''));
});

readStream.on('error', (error) => {
    console.log(error);
});*/

/*
fs.createReadStream('./12-stream/file.txt', {
    /!*highWaterMark: 64,   // 기본 64 kbytes 읽고 쓴다.
    encoding: 'utf-8',*!/
}).on('data', (chunk) => {
    // console.log(chunk);
    data.push(chunk);
    console.count('data');
}).on('end', () => {
    console.log(data.join(''));
}).on('error', (error) => {
    console.log(error);
});*/

fs.createReadStream('./12-stream/file.txt', {
    highWaterMark: 8,   // 기본 64 kbytes 읽고 쓴다.
    /* encoding: 'utf-8',*/
}).once('data', (chunk) => {
    // console.log(chunk);
    data.push(chunk);
    // console.count('data');
}).on('end', () => {
    console.log(data.join(''));
}).on('error', (error) => {
    console.log(error);
});