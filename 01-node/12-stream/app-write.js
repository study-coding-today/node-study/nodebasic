const fs = require('fs');

const writeStream = fs.createWriteStream('./12-stream/file3.txt');
writeStream.on('finish', () => {
    console.log('finished!');
});

writeStream.write('hello\n');
writeStream.write('javascript');
writeStream.end();