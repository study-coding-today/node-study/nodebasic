const process = require('process');

/* 현재실행되는 노드의 위치 */
console.log(process.execPath);
/* 노드 버전 */
console.log(process.version);
/* 프로세스 id */
console.log(process.pid);
/* 부모프로세스 id */
console.log(process.ppid);
/* 현 플랫폼 정보 */
console.log(process.platform);
/* 컴에 환경정보  */
console.log(process.env);
/* 얼마나 실행 되었나 */
console.log(process.uptime());
/* 현재 경로 */
console.log(process.cwd());
/* 사용량 */
console.log(process.cpuUsage());

setTimeout(() => {
    console.log("setTimeout");
}, 0);
/* nextTick : callback 함수를 task queue 에 넣어라*/
process.nextTick(() => {
    console.log('nextTick');
});

for (let i = 0; i < 30; i++) {
    console.log('for looping.....');
}