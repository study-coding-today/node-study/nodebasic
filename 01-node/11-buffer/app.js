const buf = Buffer.from('Hi');
console.log(buf);           // unicode로 출력
console.log(buf.length);
console.log(buf[0]);        // ASCII로 출력
console.log(buf[1]);
console.log(buf.toString());

const buf2 = Buffer.alloc(2);       // memory를 초기화하고 확보
const buf3 = Buffer.allocUnsafe(2); // memory를 초기화하지 않고 확보
buf2[0] = 72;
buf2[1] = 105;
buf2.copy(buf3);
console.log(buf2.toString());
console.log(buf3.toString());

/* concat */
const newBuf = Buffer.concat([buf, buf2, buf3]);
console.log(newBuf.toString());


